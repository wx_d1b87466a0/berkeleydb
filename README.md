#berkeleydb
Berkeley DB是由美国Sleepycat Software公司开发的一套开放源代码的嵌入式数据库管理系统（已被Oracle收购），它为应用程序提供可伸缩的、高性能的、有事务保护功能的数据管理服务。

官方地址为：http://www.oracle.com/technology/products/berkeley-db/db/index.html

Berkeley DB是一个开源的文件数据库，介于关系数据库与内存数据库之间，使用方式与内存数据库类似，它提供的是一系列直接访问数据库的函数，而不是像关系数据库那样需要网络通讯、SQL解析等步骤。
Berkeley DB是历史悠久的嵌入式数据库系统，主要应用在UNIX/LINUX操作系统上，其设计思想是简单、小巧、可靠、高性能。
Berkeley DB (DB)是一个高性能的，嵌入数据库编程库，和C语言，C++，Java，Perl，Python，PHP，Tcl以及其他很多语言都有绑定。Berkeley DB可以保存任意类型的键/值对，而且可以为一个键保存多个数据。Berkeley DB可以支持数千的并发线程同时操作数据库，支持最大256TB的数据，广泛
用于各种操作系统包括大多数Unix类操作系统和Windows操作系统以及实时操作系统。

值得注意的是DB是嵌入式数据库系统，而不是常见的关系/对象型数据库，对SQL语言不支持，也不提供数据库常见的高级功能，如存储过程，触发器等。
特点
1. 访问速度快
2. 省硬盘空间
Berkeley DB可以轻松支持上千个线程同时访问数据库，支持多进程、事务等特性。
Berkeley DB运行在大多数的操作系统中，例如大多数的UNIX系统， 和windows系统，以及实时操作系统。
　　Berkeley DB 还拥有对一些老的UNIX数据库，例如dbm, ndbm und hsearch的兼容接口.
　　对于在java系统中的使用，Berkeley DB提供了一个压缩成jar单个文件的java版本。 这个版本可以运行在java虚拟机上使用，并且拥有和C语言版本相同的所有操作和功能。
Berkeley DB XML，是一个接口，通过它可以实现对XML数据存贮的支持。对XML数据的访问，会使用相应的查询语句如Xquery, Xpath。
Berkeley DB只支持单一的数据结构，它的所有数据包括两个部分：key 和　data.
Berkeley DB原则上是为嵌入式数据库设计的。
萝莉八所的介绍了一下，其实也是让大家有个初步认知，至少知道它是一个数据库，但是有区别于关系数据库，也就是说跟我们平常用的SQL server等关系数据库不一样，没有SQL语句操作，但是他的并发访功能超级强大。还有最关键是它支持一键多值。
好了，给个下载地址吧：http://www.linuxfromscratch.org/blfs/view/svn/server/db.html

1、安装Berkeley DB

# cd /root
# tar -zxvf db-5.3.32.tar.gz
# cd db-5.3.32
# cd build_unix

Berkeley DB默认是安装在/usr/local/BerkeleyDB.5.3目录下，其中5.3就是版本号，你也可以指定–prefix参数来设置安装目录。

# ../dist/configure
或
# ../dist/configure --prefix=/usr/local/berkeleydb --enable-cxx

其中–enable-cxx就是编译C++库，这样才能编译Berkeley DB数据库的PHP扩展php_db4。

# make
# make install

# echo '/usr/local/berkeleydb/lib/' >> /etc/ld.so.conf
# ldconfig

这2句的作用就是通知系统Berkeley DB的动态链接库在/usr/local/berkeleydb/lib/目录。

配置/etc/ld.so.conf文件
ld.so.conf文件配置了需要读入告诉缓存中的动态函数库所在目录
重新配置ld.so.conf后，在命令行执行ldconfig命令生效

该软件默认是安装在/usr/local/BerkeleyDB.4.2目录下。安装完成后，要把/usr/local/BerkeleyDB.4.2/lib的库路径加到/etc/ld.so.conf文件内，添加完成后执行一次ldconfig，使配置文件生效。这样编译openldap时才能找到相应的库文件。

ld.so.conf是什么东西？它就是系统动态链接库的配置文件。此文件内,存放着可被LINUX共享的动态链接库所在目录的名字(系统目录/lib,/usr/lib除外)，各个目录名间以空白字符(空格，换行等)或冒号或逗号分隔。一般的LINUX发行版中，此文件均含一个共享目录/usr/X11R6/lib，为X window窗口系统的动态链接库所在的目录。 ldconfig是它的管理命令，具体操作方法可查询man手册

ldconfig是个什么东东吧 ：

它是一个程序，通常它位于/sbin下，是root用户使用的东东。具体作用及用法可以man ldconfig查到
简单的说，它的作用就是将/etc/ld.so.conf列出的路径下的库文件 缓存到/etc/ld.so.cache 以供使用
因此当安装完一些库文件，(例如刚安装好glib)，或者修改ld.so.conf增加新的库路径后，需要运行一下/sbin/ldconfig
使所有的库文件都被缓存到ld.so.cache中，如果没做，即使库文件明明就在/usr/lib下的，也是不会被使用的，结果
编译过程中抱错，缺少xxx库，去查看发现明明就在那放着，搞的想大骂computer蠢猪一个。

补充：linux源码安装的三步曲

从源码安装程序时,需要依此执行以下步骤:
./configure
make
make install
他们的含义:
这些都是典型的使用GNU的AUTOCONF和AUTOMAKE产生的程序的安装步骤。
./configure是用来检测你的安装平台的目标特征的。比如它会检测你是不是有CC或GCC，并不是需要CC或GCC，它是个shell脚本
make是用来编译的，它从Makefile中读取指令，然后编译。
make install是用来安装的，它也从Makefile中读取指令，安装到指定的位置。 

