#!/usr/bin/python
import random
import string
import os,sys

from utils import utils

try:  
    from bsddb import db  
except ImportError:  
    from bsddb3 import db  

def irecords(curs):  
    record = curs.first()  
    while record:  
        yield record  
        record = curs.next()  


if __name__=='__main__':

    m_utils = utils()
    #source_path = "/home/lanve/test_data_1/"
    source_path = "/home/lanve/test_data/"
    #source_path = "../input/common/"
    #test listdir()
    print "====listdir====\n"
    m_utils.listdir(source_path,'test.txt')

    #test read file object
    #print "====test read file object====\n"
    #all_text= m_utils.read_file_object('test.txt')
    #print all_text

    #test read keyword dict
    #print "====test read keyword dict====\n"
    #m_dict = m_utils.read_keyword('test.txt')
    #print m_dict



    print db.DB_VERSION_STRING  
    adb = db.DB()  
    #adb.open('db_filename_4',dbtype = db.DB_HASH, flags = db.DB_CREATE)  
    adb.open('db_filename_5',dbtype = db.DB_BTREE, flags = db.DB_CREATE)  
    #for id in range(1,100000000):
    #    name = random.choice(['steve','koby','owen','tody','rony','steve1'])
    #    sex = random.choice(['male','female'])
        #host = random.choice('abcdefghijklmnopqrstuvwxyz!@#$%^&*()')
    #    host = string.join(random.sample(['z','y','x','w','v','u','t','s','r','q','p','o','n','m','l','k','j','i','h','g','f','e','d','c','b','a','','0','1','2','3','4','5','6','7','8','9','.'], 36)).replace(' ','')
        #adb.put(name,sex)   
        #adb.put(str(id),name)   
    #    adb.put( host , str(id))   
        #print {'id':id,'name':name,'sex':sex}
    
    index = 1
    file_list_object = open('test.txt')
    try:
        for file_line in file_list_object:
            file_url_object = open( source_path + file_line.strip('\n') )
            try:
                #all_the_text = file_url_object.read()
                for line in file_url_object:
                    host=line[1:].strip('\n')
                    adb.put(host,str(index))
                    index += 1
                    #print index,host

            finally:
                file_url_object.close()
            
            print "write a file :",index, " ", file_line
    finally:
        file_list_object.close()

    print index
    #for i,w in enumerate('some word for example'.split()):  
    #    adb.put(w,str(i))  
    
    #adb.get_size( 'zvkarhcm60f5b.qisuwnodj3gpl74y8xt1e2' )
         
    #for key, data in irecords(adb.cursor()):  
    #    print key,data  
    adb.close()  
    #print '*'*60 
