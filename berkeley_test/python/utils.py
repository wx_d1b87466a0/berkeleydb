#coding=utf-8

'''
Created on Oct 18, 2012

@author: lanvelv
'''

import os,sys

class utils():

    #list all file from a dir
    def listdir(self,m_dir,m_file):
        m_file = open( m_file ,'w')
    #    m_file.write(m_dir +'\n')
        fielnum =0
        m_list = os.listdir(m_dir)#列出目录下的所有文件和目录
        for line in m_list:
            filepath = os.path.join(m_dir,line)
            if os.path.isdir(filepath):#如果filepath是目录，则再列出该目录下的所有文件
                m_file.write(line +'//'+'\n')
                for li in os.listdir(filepath):
                    m_file.write(li +'\n')
                    fielnum = fielnum +1
            elif os.path:#如果filepath是文件，直接列出文件名
                m_file.write(line +'\n')
            
            fielnum = fielnum +1
    #    m_file.write('all the file num is '+ str(fielnum))
        m_file.close()
    
        return 
    
    #define organs_keyword_multi_pattern_search 
    #m_file_object is a file content
    #m_keyword is a dict of keywords
    def read_file_object (self, m_file_name ):
        
        file_object = open( m_file_name)
        try:
            all_the_text = file_object.read( )
        finally:
            file_object.close( )
    
        return  all_the_text
    
    
    #define organs_keyword_multi_pattern_search 
    #m_file_object is a file name
    #return a dict
    def read_keyword ( self, m_file_name ):
            
        m_dict={}
        file_object = open( m_file_name )
        try:
            for line in file_object:
                m_dict[ line.strip('\n') ] = 0 
        finally:
            file_object.close( )
    
        return m_dict

