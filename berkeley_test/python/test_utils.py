#coding=utf-8

'''
Created on Oct 18, 2012

@author: lanvelv
'''


import os,sys

from utils import utils

if __name__=='__main__':

    m_utils = utils()

    source_path = "../input/"
    #source_path = "../input/common/"
    #test listdir()
    print "====listdir====\n"
    m_utils.listdir(source_path,'test.txt')

    #test read file object
    print "====test read file object====\n"
    all_text= m_utils.read_file_object('test.txt')
    print all_text

    #test read keyword dict
    print "====test read keyword dict====\n"
    m_dict = m_utils.read_keyword('test.txt')
    print m_dict
