try:  
    from bsddb import db  
except ImportError:  
    from bsddb3 import db  
print db.DB_VERSION_STRING  
  
def irecords(curs):  
    record = curs.first()  
    while record:  
        yield record  
        record = curs.next()  
          
adb = db.DB()  
adb.open('db_filename_2',dbtype = db.DB_HASH, flags = db.DB_CREATE)  
for i,w in enumerate('some word for example'.split()):  
    adb.put(w,str(i))  
      
for key, data in irecords(adb.cursor()):  
    print key,data  
adb.close()  
print '*'*60 
