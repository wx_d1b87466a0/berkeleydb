#include <db.h>
#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef _WIN32
extern int getopt(int, char * const *, const char *);
#else
#include <unistd.h>
#endif

#include <db.h>

#define DATABASE        "access.db"

void clean_string(char *str);

int main(int argc,char *argv[])
{
        extern int optind;
        DB *dbp;
        DBC *dbcp;
        DBT key, data;
        size_t len;
        int ch, ret, rflag,index;
        char *database, *p, *t, buf[1024], rbuf[1024];
        const char *progname = "access";                /* Program name. */
        FILE *list_fd;
        FILE *url_fd;
        char temp_file[100];
        char host[100];

        u_int32_t flags; /* database open flags */
        index = 0;

        ret = db_create(&dbp, NULL, 0);
        if (ret != 0) 
        {
            printf("create db error\n");
            return -1;
        }
        
        if ((ret = dbp->set_pagesize(dbp, 1024)) != 0) 
        {
	    dbp->err(dbp, ret, "set_pagesize");
            return -1;
	}
	if ((ret = dbp->set_cachesize(dbp, 0, 6400 * 102400, 0)) != 0) 
        {
	    dbp->err(dbp, ret, "set_cachesize");
            return -1;
	}  
        flags = DB_CREATE; 
        //ret = dbp->open(dbp, NULL, "my_db_2.db", NULL, DB_BTREE, flags,  0); 
        ret = dbp->open(dbp, NULL, "my_db_2.db", NULL, DB_BTREE, flags,  0664); 
        if (ret != 0) 
        {
            printf("open db error\n");
            return -1;
        }

        list_fd = fopen("./test.txt","r");
        if( list_fd == NULL )
        {
            printf("open test.txt error\n");
            return -1;
        }
        while(fgets(buf,100,list_fd) != NULL)
        {
           // if( buf[strlen(buf)-1] =='\n' )
            //buf[strlen(buf) - 1 ]='\0';
            clean_string(buf);
            sprintf(temp_file,"/home/lanve/test_data/%s",buf);
            //printf("buf:%s,len:%d\n",buf,strlen(buf));
            //printf("2222\n");
            //printf("temp_file:%s\n",temp_file);
            //return -1;
            
            url_fd = fopen(temp_file,"r");
            if(url_fd == NULL)
            {
                printf("open file %s error\n",temp_file);
                return -1;
            }
            while(fgets(buf,100,url_fd) != NULL)
            {
                index++;
                //ch=strchr(' ');
                clean_string(buf);
              //  printf("buf:%s,len:%d\n",buf,strlen(buf));
              //  return -1;
                strncpy(rbuf,buf+2,strlen(buf));
        //        printf("buf:%s,len:%d\n",rbuf,strlen(rbuf));
        //        return -1;
                //strncpy(key.data,rbuf,strlen(rbuf));
                //key.data = "hello,world";
                //data.data = rbuf;
                //data.data = rbuf;
                memset(&key,0x0,sizeof(key));
                memset(&data,0x0,sizeof(data));
                key.data = rbuf;
                key.size = strlen(rbuf)+1;
                data.data = "123";
                data.size = strlen("123")+1;
                //switch (ret = dbp->put(dbp, NULL, &key, &data, DB_NOOVERWRITE))
                ret = dbp->put(dbp, NULL, &key, &data, DB_NOOVERWRITE);
                //ret = dbp->put(dbp, NULL, &key, &data, 0);
                if (ret != 0)
                {
                    //printf("put data error\n");
                    dbp->err(dbp,ret,"DB->PUT");
                //        dbp->err(dbp, ret, "DB->put");
                //        if (ret == DB_KEYEXIST)
                //            printf("date exist\n");
                        //break;
                }
                //status = db->Put(leveldb::WriteOptions(), host,"test");


                //cout<< index++ << " " <<host<< endl;
            }
            fclose(url_fd);
            printf("Write a file success,fileName:%s   index : %d \n",temp_file, index);
        }
        fclose(list_fd);
}

/*
去掉字符串首尾的 \x20 \r \n 字符
by sincoder
*/
void clean_string(char *str)
{
    char *start = str - 1;
    char *end = str;
    char *p = str;
    while(*p)
    {
        switch(*p)
        {
        case ' ':
        case '\r':
        case '\n':
            {
                if(start + 1==p)
                    start = p;
            }
            break;
        default:
            break;
        }
        ++p;
    }
    //现在来到了字符串的尾部 反向向前
    --p;
    ++start;
    if(*start == 0)
    {
        //已经到字符串的末尾了 
        *str = 0 ;
        return;
    }
    end = p + 1;
    while(p > start)
    {
        switch(*p)
        {
        case ' ':
        case '\r':
        case '\n':
            {
                if(end - 1 == p)
                    end = p;
            }
            break;
        default:
            break;
        }
        --p;
    }
    memmove(str,start,end-start);
    *(str + (int)end - (int)start) = 0;
}
