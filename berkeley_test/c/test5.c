#include     <stdlib.h>
#include     <string.h>
#include     <db.h>

int main(int argc,char *argv[])
{
    DB *dbp;
    DBT key,data;
    int ret;
    if( (ret = db_create(&dbp,NULL,0)) != 0){
          fprintf(stderr,"db_create:%s! \n",db_strerror(ret));
          exit (1);
    }

    if( (ret = dbp->open(dbp,NULL,argv[1],NULL,DB_BTREE,DB_CREATE,0664)) != 0){
          dbp->err(dbp,ret,"%s!",argv[1]);
    }
    memset(&key,0x0,sizeof(key));
    memset(&data,0x0,sizeof(data));
    key.data = "sport";
    key.size = sizeof("sport");

    data.data = "football";
    data.size = sizeof("football");

    //if( (ret = dbp->put(dbp,NULL,&key,&data,0)) == 0)
    if( (ret = dbp->put(dbp,NULL,&key,&data,DB_NOOVERWRITE)) == 0)
    {
          fprintf(stderr,"db:%s key stored ret :%d .\n",(char *)key.data,ret);
    }else
    {
          dbp->err(dbp,ret,"DB->PUT");
    }

    dbp->close(dbp,0);


    return 0;
}
