#include <stdio.h>  
#include <db.h>  
int main()  
{  
    DB* dbp;  
    u_int32_t flags;  
    int ret;  
    ret = db_create(&dbp, NULL, 0);  
    if (ret != 0)  
    {  
        printf("create db error\n");
        return -1;
    }  
    flags = DB_CREATE;  
    ret = dbp->open(dbp, NULL, "my_db_1.db", NULL, DB_BTREE, flags, 0);  
    if (ret != 0)  
    {  
        printf("open db error");
        return -1;
    }  
    if (dbp != NULL)  
    {  
        dbp->close(dbp, 0);  
    }  
      
    return 0;  
}
