/*-
 * See the file LICENSE for redistribution information.
 *
 * Copyright (c) 1997, 2013 Oracle and/or its affiliates.  All rights reserved.
 *
 * $Id$
 */

#include <sys/types.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#ifdef _WIN32
extern int getopt(int, char * const *, const char *);
#else
#include <unistd.h>
#endif

#include <db.h>

#define	DATABASE	"access.db"

int main(int argc,char *argv[])
{
	extern int optind;
	DB *dbp;
	DBC *dbcp;
	DBT key, data;
	size_t len;
	int ch, ret, rflag,index;
	char *database, *p, *t, buf[1024], rbuf[1024];
	const char *progname = "access";		/* Program name. */
        FILE *list_fd;
        FILE *url_fd;
        char temp_file[100];
        char host[100];

	rflag = 0;
        index = 0;

	/* Create and initialize database object, open the database. */
	if ((ret = db_create(&dbp, NULL, 0)) != 0) 
        {
            fprintf(stderr, "%s: db_create: %s\n", progname, db_strerror(ret));
	    return (EXIT_FAILURE);
	}
//	dbp->set_errfile(dbp, stderr);
//	dbp->set_errpfx(dbp, progname);
//	if ((ret = dbp->set_pagesize(dbp, 1024)) != 0) 
//       {
//		dbp->err(dbp, ret, "set_pagesize");
//		goto err1;
//	}
//	if ((ret = dbp->set_cachesize(dbp, 0, 32 * 1024, 0)) != 0) 
//       {
//		dbp->err(dbp, ret, "set_cachesize");
//		goto err1;
//	}
        printf("3333\n");
	if ((ret = dbp->open(dbp, NULL, "aaaccess.db", NULL, DB_BTREE, DB_CREATE, 0664)) != 0) 
        {
            printf("open db error\n");
            return;
	}

	/*
	 * Insert records into the database, where the key is the user
	 * input and the data is the user input in reverse order.
	 */
	//memset(&key, 0, sizeof(DBT));
	//memset(&data, 0, sizeof(DBT));
        printf("4444");
        list_fd = fopen("./test.txt","r");
        while(fgets(buf,100,list_fd) != NULL)
        {
    
            sprintf(temp_file,"/home/lanve/test_data/%",buf);
            url_fd = fopen(temp_file,"r"); 
            while(fgets(buf,100,list_fd) != NULL)
            {
                strncpy(key.data,buf,strlen(buf)); 
//		key.data = ;
		data.data = rbuf;
		data.size = key.size = (u_int32_t)len - 1;
		switch (ret = dbp->put(dbp, NULL, &key, &data, DB_NOOVERWRITE)) 
                {
		    case 0: break;
                    default: 
                        dbp->err(dbp, ret, "DB->put");
       			if (ret != DB_KEYEXIST)
			   goto err1;
			break;
		}
                //status = db->Put(leveldb::WriteOptions(), host,"test");
           
                index++;
    
                //cout<< index++ << " " <<host<< endl;
            }
            fclose(url_fd);
        }
        fclose(list_fd); 
//	/* Acquire a cursor for the database. */
//	if ((ret = dbp->cursor(dbp, NULL, &dbcp, 0)) != 0) {
//		dbp->err(dbp, ret, "DB->cursor");
//		goto err1;
//	}
//
//	/* Initialize the key/data pair so the flags aren't set. */
//	memset(&key, 0, sizeof(key));
//	memset(&data, 0, sizeof(data));
//
//	/* Walk through the database and print out the key/data pairs. */
//	while ((ret = dbcp->get(dbcp, &key, &data, DB_NEXT)) == 0)
//		printf("%.*s : %.*s\n",
//		    (int)key.size, (char *)key.data,
//		    (int)data.size, (char *)data.data);
//	if (ret != DB_NOTFOUND) {
//		dbp->err(dbp, ret, "DBcursor->get");
//		goto err2;
//	}
//
	/* Close everything down. */
	if ((ret = dbcp->close(dbcp)) != 0) {
		dbp->err(dbp, ret, "DBcursor->close");
		goto err1;
	}
	if ((ret = dbp->close(dbp, 0)) != 0) {
		fprintf(stderr,
		    "%s: DB->close: %s\n", progname, db_strerror(ret));
		return (EXIT_FAILURE);
	}
	return (EXIT_SUCCESS);

err2:	(void)dbcp->close(dbcp);
err1:	(void)dbp->close(dbp, 0);
	return (EXIT_FAILURE);
}
